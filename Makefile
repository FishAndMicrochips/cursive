BINARYPATH = /usr/local/bin
build:
	sbcl --load ./make.lisp
install:
	mkdir -p ${BINARYPATH}
	cp -f cursive ${BINARYPATH}
	chmod 755 ${BINARYPATH}
