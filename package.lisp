;;;; package.lisp

(defpackage #:cursive
  (:use #:cl)
  (:export :main :cursify))
