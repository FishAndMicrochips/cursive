;;;; cursive.lisp

(in-package #:cursive)

(defparameter *map* nil)

(setf *map*
      (loop
        with table = (make-hash-table)
        for key across "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVQXYZ"
        for fancy across "𝒶𝒷𝒸𝒹𝑒𝒻𝑔𝒽𝒾𝒿𝓀𝓁𝓂𝓃𝑜𝓅𝓆𝓇𝓈𝓉𝓊𝓋𝓌𝓍𝓎𝓏𝒜𝐵𝒞𝒟𝐸𝐹𝒢𝐻𝐼𝒥𝒦𝐿𝑀𝒩𝒪𝒫𝒬𝑅𝒮𝒯𝒰𝒱𝒬𝒳𝒴𝒵"
        do (setf (gethash key table)
                 fancy)
        finally (return table)))

(defun cursify-character (c)
  (or (gethash c *map*)
     c))

(defun cursify (s)
  (map '(vector character) #'cursify-character s))

(defun my-command-line ()
  (or
   #+CLISP *args*
   #+SBCL sb-ext:*posix-argv*
   #+LISPWORKS system:*line-arguments-list*
   #+CMU extensions:*command-line-words*
   nil))

(defun input-string ()
  (format nil "~{~a~^ ~}" (rest (my-command-line))))

(defun main ()
  (format t "~A~%" (cursify (input-string))))
