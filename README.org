* About
  This is a simple project to learn how to compile common lisp ASDF projects. It's as simple as that.
  The project is small enough for me to not really care about different implementations or different architectures.

* Dependencies
  - ASDF

  Currently, the implementation is hard-coded to SBCL in the makefile, but all SBCL does is load /make.lisp/.
  Changing the implementation should be easy.
* Installation
  #+begin_src
    $ make
    # make install
  #+end_src
* Usage
  #+begin_src
    $ cursive Hello World!
    𝒽𝑒𝓁𝓁𝑜 𝓌𝑜𝓇𝓁𝒹!
  #+end_src
