;;;; cursive.asd

(asdf:defsystem #:cursive
  :description "Simple thing that writes fancy"
  :author "Amber G (fishandmicrochips <at> disroot <dot> org)"
  :license  "GNU Affero GPL V3"
  :version "0.0.1"
  :serial t
  :build-operation "program-op"
  :build-pathname "cursive"
  :entry-point "cursive:main"
  :components ((:file "package")
               (:file "cursive")))
